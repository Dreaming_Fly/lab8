
import random as random
from math import sqrt
import timeit


# Функция отбора точек для задонного радиуса
def point(x_0, y_0, radius, points):
    uyp = 0
    for x, y in points:
        delta = sqrt((x - x_0) ** 2 + (y - y_0) ** 2)
        if delta <= radius:
            uyp += 1
    return uyp


while True:  # Выбор че делать
    try:
        print('Вывести время точек от 100к до 1 млн с шагом 100к?')
        num = int(input('[1]:Ввод свои значений\n'
                        '[2]:Подсчитать время\n'))
        if num > 2 or num < 1:
            print('Выберите 1 или 2')
        else:
            break
    except ValueError:
        print('Не верный тип данных!')

while True:  # Ввод границ графика и рандомных чисел
    try:
        t_1 = int(input('Граница 1: '))
        t_2 = int(input('Граница 2: '))
        if t_2 < t_1:
            print('Граница 1 должна быть меньше Границы 2')
        else:
            break
    except ValueError:
        print('Введен неверный тип данных !')

if num == 1:
    while True:  # Ввод индекса точки с радиусом
        try:
            ber = int(input('Кол - во точек:'))
            z = int(input('Введите индекс точки: '))
            r = float(input('Радиус: '))
            if r <= 0:
                print('Радиус не может быть > 0')
            else:
                break
        except ValueError:
            print('Не правильный формат данных !')

    p = [(random.uniform(t_2, t_1),
          random.uniform(t_2, t_1)) for i in range(ber)]

    while True:  # Выбор точки по индексу
        try:
            x0, y0 = p[z]
            break
        except IndexError:
            print('Индекс не существует')
            exit(-1)

    qnts = point(x0, y0, r, p) # подсчет точек входящих в радиус
    print('Из', ber, 'точек входит', qnts, 'точек(ки)')


elif num == 2:
    time = 0
    file = open('Times.txt', 'w')  # открытие файла для записи
    for i in range(100000, 1000001, 100000):  # цикл создания точек в заданном радиусе
        r = random.randint(t_1, t_2) # радиус рандомно генерируется в границых т1 т2
        p = [(random.uniform(t_2, t_1),
              random.uniform(t_2, t_1)) for i in range(i)] # радномное генерирование массива координат точек
        x0, y0 = p[0] # первая точка в массиве
        for j in range(3):
            t0 = timeit.default_timer() # запуск подсчета времени
            qnts = point(x0, y0, r, p) # подсчет точек входящих в радиус
            time = timeit.default_timer() - t0 #подсчет времени работы функции
        print('Из', i, 'точек входит', qnts, 'точек(ки)')

        file.write(str(i) + ' ' + str(time / 3).replace('.', ',') + '\n')
    file.close()
